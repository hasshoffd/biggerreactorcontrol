# BiggerReactorControl

***Based off of Extreme Reactors Control - Original work thanks to the contributors over at https://gitlab.com/seekerscomputercraft/extremereactorcontrol and Reactor and Turbine control at https://github.com/ThorsCrafter/Reactor-and-Turbine-control-program***

## Description:

The following list shows the features of the program:
- Control up to 180 Turbines + Multiple Reactors (Tested with 384 Turbines and 16 Reactors on FTB Rev 3.4)
- Automatic and manual Control of Reactor and attached Turbines
- Energy-based automatic Control
    - Switches Reactor and Turbines on/off if energy level is low/high
    - Supports multiple Energy Storage typesm like Capacitorbanks (EnderIO), Energy Core (Draconic Evolution), etc.
- Large option menu
    - Change Background and Text Color
    - Set energy level for activating/deactivating the reactor
    - Set Reactor Steam Output Level
    
## How To Install
- Set up a Computer, connect all parts (Reactor, Energy Storage, Turbines) with ***Wired Modems***
- ***Activate*** all modems
- Type in the following into the computer:

    ```
    pastebin get BR1xfZeU git
    git
    ```
        
- Then follow the install instructions
